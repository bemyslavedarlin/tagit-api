-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 24 2017 г., 09:43
-- Версия сервера: 5.7.17-12-beget-log
-- Версия PHP: 5.6.30

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `bemyslaved_tagit`
--
CREATE DATABASE IF NOT EXISTS `bemyslaved_tagit` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bemyslaved_tagit`;

-- --------------------------------------------------------

--
-- Структура таблицы `requests`
--
-- Создание: Апр 24 2017 г., 06:07
-- Последнее обновление: Апр 24 2017 г., 06:42
--

DROP TABLE IF EXISTS `requests`;
CREATE TABLE IF NOT EXISTS `requests` (
  `requestid` int(11) NOT NULL AUTO_INCREMENT,
  `request_url` text,
  `request_title` text,
  `request_desc` text,
  `request_frame` text,
  `request_thumb` text,
  `request_method` varchar(16) DEFAULT NULL,
  `request_ipv` varchar(39) DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`requestid`),
  KEY `request_ipv` (`request_ipv`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Очистить таблицу перед добавлением данных `requests`
--

TRUNCATE TABLE `requests`;SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
