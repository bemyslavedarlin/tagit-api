<?php
define("INIT",	true);

/*GLOBAL CONFIGURATION*/
define("DS",		"/");
define("ROOT",		$_SERVER['DOCUMENT_ROOT'].DS);
define('SERVICES',	ROOT."services".DS);

//LOAD DEFINES AND LANG STRINGS
require_once ROOT.'libs/local/config.inc.php'; //stores constants
require_once ROOT.'libs/local/lang.inc.php'; //stores string values for errors and other message strings

//LOAD HELPERS
require_once ROOT.'libs/local/classes/model.class.php'; //stores wrapper for database operations
require_once ROOT.'libs/local/classes/getter.class.php'; //stores wrapper for curl operations

//LOAD SERVICE
require_once ROOT.'libs/local/classes/service.class.php'; //main api core, opareting with all provided data by given rules

//LOAD APPLICATION
require_once ROOT.'libs/vendor.php'; //short class to operate service and requests

//INIT APP VENDOR AND RESPONSE RESULTS
$vendor = new Vendor();

header('Content-Type: application/json; charset=utf-8');
echo $vendor->init();
