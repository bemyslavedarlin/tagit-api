<?php
defined('INIT') or die('Direct access is not allowed.');
//init language object;
$LANG = new stdClass();

/*ERRORS*/
$LANG->errors = new stdClass();
$LANG->errors->_no_error = 'Ошибок нет. Запрос отсутствует.';
$LANG->errors->_empty = 'Пустой параметр!';
$LANG->errors->_wrong = 'Параметр устанолен некорректно!';
$LANG->errors->_no_service = 'Сервис не поддерживается! Файл сервиса не найден!';
$LANG->errors->_no_rules = 'Не заданы правила обработки данных сервиса!';
$LANG->errors->_bad_url = 'Ссылка передана в неверном формате!';
$LANG->errors->_empty_responce = 'Пустой ответ сервера!';
$LANG->errors->_wrong_token = 'Передан неверный токен!';