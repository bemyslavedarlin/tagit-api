<?php
defined('INIT') or die('Direct access is not allowed.');

/*DATABASE CONFIGURATION*/
define("DB_SERVER",		'localhost');
define("DB_NAME",		'rest');
define('DB_USERNAME',	'root');
define('DB_PASSWORD',	'');

/*CURL CONFIGURATION*/
define("_RETURNTRANSFER",	true);
define("_HEADER",			false);
define('_CONNECTTIMEOUT',	true);
define('_CUSTOMREQUEST',	'GET');

/*USAGE TOKEN*/
define("TOKEN",             'aa312a17-1a9d-4f57-a2a7-589e91da6e1d');
define("DEFAULT_CHARSET",	'UTF-8');
