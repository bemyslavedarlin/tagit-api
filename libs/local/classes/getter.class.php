<?php
defined('INIT') or die('Direct access is not allowed.');

class Getter {
	
	public $content;

	/**
	 * @param string $url
	 */
	private function init($url = '')
	{
		if($url != '') return false;
			
        $options = array(
            CURLOPT_RETURNTRANSFER	=> _RETURNTRANSFER,
            CURLOPT_HEADER			=> _HEADER,
            CURLOPT_CONNECTTIMEOUT	=> _CONNECTTIMEOUT
        );

        $ch = curl_init($url);
        
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        curl_close($ch);
        return $content;
        
	}
}
