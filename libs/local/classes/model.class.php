<?php
defined('INIT') or die('Direct access is not allowed.');

class Model
{
	protected $db = null;

	function __construct()
	{ 
		try { 
			$this->db = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME.";charset=utf8", DB_USERNAME, DB_PASSWORD); 
			$this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		}  
		catch(PDOException $e) {  
			echo $e->getMessage();
		}
	}

	/**
	 * @param null $query
	 * @param null $params
	 * @param int  $method
	 * @return mixed
	 */
	public function _single($query = null, $params = null, $method = PDO::FETCH_OBJ)
	{
		$res = $this->db->prepare($query);
		$res->execute($params);
		
		return $res->fetch($method);
	}

	/**
	 * @param null $query
	 * @param null $params
	 * @return mixed
	 */
	public function _exec($query = null, $params = null)
	{
		return $this->db->prepare($query)->execute($params);
	}

	function __destruct()
	{
		$this->db = null;
	}
}