<?php
defined('INIT') or die('Direct access is not allowed.');

class Service
{

	/**
	 * list of helper variables
	 * @var bool|string
	 */
	private $url;
	private $rules;
    
	public $charset;

	/**
	 * @param $url
	 */
	function init($url)
	{
        $this->url = $url;
	}

	/**
	 * gets service ini file by provided url
	 * @param $url
	 * @return bool|string
	 */
	private function getService()
	{
		$request = parse_url($this->url);		
		$host = str_replace("www.", "", $request['host']);
        
        $service = SERVICES.$host.'.ini';
		
		return (file_exists($service)) ? $service : false;
	}
    
    private function loadRulesIni()
    {
		$arRules = parse_ini_file($this->service);
        $this->rules = $arRules ?: array();
    }

	/**
	 * loads service rules from provided service ini file and format it for dom usage
	 * TO DO CHECK RULES
	 * @return bool
	 */
	private function getRules()
	{
        if(!empty($this->rules)){
            $rules['title'] = $this->getTag($this->rules);
            $rules['description'] = $this->getTag($this->rules,'description');
            $rules['thumbnail'] = $this->getTag($this->rules,'thumbnail');
            $rules['embed'] = $this->getTag($this->rules,'embed');
            
            if($rules['embed'] != ''){
                $rules['width'] = $this->rules['width'];
                $rules['height'] = $this->rules['height'];
                $rules['embedFrame'] = $this->rules['embedFrame'];
                
                //MAIL.RU VIDEO FRAME HACK (there is no inline tags with embed video url on page)
                $rules['embedFormat'] = isset($this->rules['embedFormat']) ? $this->rules['embedFormat'] : false;
            }
            return ($rules['title'] != '') ? $rules : false;
        }
		return false;
	}
    
    private function getCharset()
    {
		return !empty($this->rules['charset'])?: DEFAULT_CHARSET;
    }

	/**
	 * form tags arrays to use while parsing data
	 * @param        $arRules
	 * @param string $type
	 * @return bool
	 */
	private function getTag($arRules,$type = 'title')
	{
		if(isset($arRules[$type])){
			$arSetting = explode(";", $arRules[$type]);
			
			if(count($arSetting)){
				
				foreach($arSetting as $strElement){
					$arElement = explode("/", $strElement);
					
					if(count($arElement)){
						$arSelector = explode("=", $arElement[1]);
						$tag[$arElement[0]] = $arSelector;
					}
				}
                return $tag;
			}
		}
		return '';
	}
}
