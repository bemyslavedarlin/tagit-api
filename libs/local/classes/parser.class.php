<?php
defined('INIT') or die('Direct access is not allowed.');

class Parser
{

	/**
	 * list of helper variables
	 * @var bool|string
	 */
	private $html;
	private $embed;

	/**
	 * list of response variables, filled during content parsing
	 * @var
	 */
	public $title;
	public $description;
	public $frame;
	public $thumbnail;

	/**
	 * @param $html
	 */
	function init($html)
	{
		$this->html = $html;
		$this->charset = $charset;
	}

	/**
	 * checks if required vars initialised
	 * @return bool
	 */
	public function check()
	{
		return (!empty($this->title) && !empty($this->frame));
	}

	/**
	 * responses result array or error/usage array formatted to json
	 * @return array
	 */
	public function response()
	{
		if(!$this->check()) return;
        
        return array(
            'title' => $this->title,
            'description' => $this->description,
            'thumbnail' => $this->thumbnail,
            'frame' => $this->frame,
        );
	}

	/**
	 * parses given contents from url by DOM lib
	 * @return bool
	 */
	private function parseHtml($rules)
	{
        //init DOMDocument
		$doc = new DOMDocument();
		libxml_use_internal_errors(true);   //=>>
		$doc->loadHTML($this->html);        //libxml errors wrapper needed to ignore html5 unknown tags errors
		libxml_clear_errors();              //<<=
		
		//setting title and description nodes:
		$this->getMetas($doc,$rules);
		
		$this->embed = (isset($rules['embed'])) ? $this->getOptional($doc,$rules['embed']) : '';
		$this->thumbnail = (isset($rules['thumbnail'])) ? $this->getOptional($doc,$rules['thumbnail']) : '';
		
		//MAIL.RU VIDEO FRAME HACK (there is no inline tags with embed video url on page)
		$this->embed = isset($rules['embedFormat']) ? str_replace("{LAST}", end(explode("/",$this->embed)), $rules['embedFormat']) : $this->embed;
        
        if(!empty($this->embed)){
            $search = array('{WIDTH}', '{HEIGHT}', '{SRC}');
            $replace = array($rules['width'], $rules['height'], $this->embed);
            
            $this->frame = str_replace($search, $replace, $rules['embedFrame']);
        }
		
		return $this->check();
	}

	/**
	 * format parsed contents to readable normalized view
	 * @return bool
	 */
	private function formatData()
	{
		$this->title = iconv("UTF-8", $this->charset, $this->title);
		$this->description = iconv("UTF-8", $this->charset, $this->description);
		
		return (!empty($this->title) && !empty($this->description));
	}

	/**
	 * get main meta tags as title and description from provided by DOM html doc
	 * i assume there are more elegant solutions to get meta tags such as get_meta_tags($url)
	 * @param $doc
	 */
	private function getMetas($doc,$rules)
	{
		$nodes = $doc->getElementsByTagName('title');
		$this->title = ($nodes->length>0) ? $nodes->item(0)->nodeValue : '';
		if($this->title == ''){
			$nodes = $doc->getElementsByTagName('h1');
			
			$this->title = ($nodes->length>0) ? $nodes->item(0)->nodeValue : $this->title;
			$this->title = ($this->title == '' && $rules['title']) ? $this->get_optional($doc,$rules['title']) : $this->title;
		}
		
		$metas = $doc->getElementsByTagName('meta');
		for ($i = 0; $i < $metas->length; $i++){
			$meta = $metas->item($i);
			
			if($meta->getAttribute('name') == 'description'){
				
				$this->description = $meta->getAttribute('content');
				$this->description = ($this->description == '' && $rules['description']) ? $this->get_optional($doc,$rules['description']) : $this->description;
			}
			
			if($meta->getAttribute('name') == 'keywords'){
				$this->keywords = $meta->getAttribute('content');
			}
		}
	}

	/**
	 * get optional tags values from provided by DOM html doc
	 * @param $doc
	 * @param $tagRules
	 * @return mixed
	 */
	private function getOptional($doc,$tagRules)
	{
		foreach($tagRules as $tag => $selector){
			$elements = $doc->getElementsByTagName($tag);
			
			for ($i = 0; $i < $elements->length; $i++){
				$element = $elements->item($i);
				
				if($element->getAttribute($selector[0]) == $selector[1]){
					return ($selector[2] != "text") ? $element->getAttribute($selector[2]) : $element->nodeValue;
				}
			}
		}
        return;
	}
}
