<?php
defined('INIT') or die('Direct access is not allowed.');

class Vendor
{ 
	private $request;
	private $method;

	/**
	 * gets request and returns json data or usage/error data as result
	 * @return json_encoded string
	 */
	function init()
	{
		global $LANG;
		
		$this->get_request();
		$usage = Service::usage_rules();
		
		if(isset($this->request)){
			
			if(isset($this->request->token) && $this->request->token == TOKEN){
				
				if(isset($this->request->url)){
					$model = new Model();
					$response = $this->get_response_db($model);
					
					if(count(array_filter($response)) <= 0){
						$service = new Service($this->request->url);
						
						if($service->check()){
							$response = $service->response();
							$this->store_request($model,$response);
						}else{
							return $this->erresponse($usage,$LANG->errors->_empty_responce);
						}
					}
					
					return $this->erresponse(false,false,$response);
					
				}else{
					return $this->erresponse($usage,$LANG->errors->_bad_url);
				}
			}else{
				return $this->erresponse($usage,$LANG->errors->_wrong_token);
			}
		}
		
		return $this->erresponse($usage,$LANG->errors->_no_error);
	}
	
	private function erresponse($usage,$error,$raw = '')
	{
		if(!empty($raw) && !$usage && !$error) $response = $raw;
		else $response = array( "errors" => $error, "usage_example" => $usage);
		return json_encode(
			$response,
			JSON_UNESCAPED_UNICODE
		);
	}
	

	/**
	 * stores obtained results into db
	 */
	private function get_response_db($model = null)
	{
		$query = "SELECT * FROM `requests` WHERE `request_url` = ?";
		$result = $model->_single($query,array(base64_encode($this->request->url)),PDO::FETCH_OBJ);
		return array(
			'title' => base64_decode($result->request_title),
			'description' => base64_decode($result->request_desc),
			'thumbnail' => base64_decode($result->request_thumb),
			'frame' => base64_decode($result->request_frame),
		);
	}
	

	/**
	 * stores obtained results into db
	 */
	private function store_request($model = null,$response = null)
	{
		if($model && $response){
			$query = "
				INSERT INTO `requests`(
					`request_url`, 
					`request_title`, 
					`request_desc`, 
					`request_frame`, 
					`request_thumb`, 
					`request_method`, 
					`request_ipv`
				) VALUES ( ?, ?, ?, ?, ?, ?, ? )
			";
			$ipAddress = $_SERVER['REMOTE_ADDR'];
			if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
				$ipAddress = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
			}
			$params = array(
				base64_encode($this->request->url),
				base64_encode($response['title']),
				base64_encode($response['description']),
				base64_encode($response['frame']),
				base64_encode($response['thumbnail']),
				$this->method,
				$ipAddress
			);
			$model->_exec($query,$params);
		}
	}

	/**
	 * analyzes provided request for one of available methods and put request data into local variable
	 */
	private function get_request()
	{
		$this->method = $_SERVER['REQUEST_METHOD'];
		switch ($this->method){
			case 'GET':
				$this->request = (object)$_GET;
				break;
			case 'POST':
				$this->request = (object)$_POST;
				break;
			case 'PUT':
				//HACK TO SET $_PUT GLOBAL ARRAY
				$_PUT = array();
				$putdata = file_get_contents('php://input');
				$exploded = explode('&', $putdata);

				foreach($exploded as $pair) {
					$item = explode('=', $pair);
					if(count($item) == 2) {
						$_PUT[urldecode($item[0])] = urldecode($item[1]);
					}
				}
				$this->request = (object)$_PUT;
				break;
			case 'DELETE':
				//HACK TO SET $_DELETE GLOBAL ARRAY
				$_DELETE = array();
				parse_str(file_get_contents('php://input'),$_DELETE);
				$this->request = (object)$_DELETE;
				break;
		}
	}
}