<?php
defined('INIT') or die('Direct access is not allowed.');

class Service
{

	/**
	 * list of helper variables
	 * @var bool|string
	 */
	private $service;
	private $rules;
	private $html;
	private $charset;
	private $error;
	private $_errors;

	/**
	 * list of respose variables, filled during content parsing
	 * @var
	 */
	public $title;
	public $description;
	public $embed;
	public $frame;
	public $thumbnail;

	/**
	 * in my opinion, fastest way to stick all class methods together is its constructor
	 * @param $url
	 */
	function __construct($url)
	{
		global $LANG;
		$this->_errors = $LANG->errors;
		if (filter_var($url, FILTER_VALIDATE_URL)){
			$this->service = $this->get_service($url);
			
			if($this->service){
				$this->html = $this->get_html($url);
				
				if($this->html->content != ''){
					$this->rules = $this->load_rules();
					
					if($this->rules){						
						if($this->parse_html()){
							$this->format_data();
						}
						
					}else{
						$this->error = $this->_errors->_no_rules;
					}
					
				}else{
					$this->error = $this->_errors->_empty_responce;
				}
				
			}else{
				$this->error = $this->_errors->_no_service;
			}
			
		}else{
			$this->error = $this->_errors->_bad_url;
		}
	}

	/**
	 * checks if required vars initialised
	 * @return bool
	 */
	public function check()
	{
		return (!empty($this->title) && !empty($this->embed));
	}

	/**
	 * responses result array or error/usage array formatted to json
	 * @return array
	 */
	public function response()
	{
		if($this->check()){
			return array(
				'title' => $this->title,
				'description' => $this->description,
				'thumbnail' => $this->thumbnail,
				'frame' => $this->frame,
			);
		}else{
			return array('error' => $this->error, 'usage_example' => $this->usage_rules());
		}
	}

	/**
	 * gets service ini file by provided url
	 * @param $url
	 * @return bool|string
	 */
	private function get_service($url)
	{
		$request = parse_url($url);
		$query = $request['query'];
		parse_str($query, $params);
		
		$host = str_replace("www.", "", $request['host']);
		
		if(file_exists(SERVICES.$host.'.ini')){
			return SERVICES.$host.'.ini';
		}
		return false;
	}

	/**
	 * initializes Getter object to load contents from provided url by curl
	 * @param $url
	 * @return Getter
	 */
	private function get_html($url)
	{
		return new Getter($url);
	}

	/**
	 * loads service rules from provided service ini file and format it for dom usage
	 * TO DO CHECK RULES
	 * @return bool
	 */
	private function load_rules()
	{
		$arRules = parse_ini_file($this->service);
		$this->charset = $arRules['charset'];
		$this->tags['title'] = $this->get_tag($arRules,$type = 'title');
		$this->tags['description'] = $this->get_tag($arRules,$type = 'description');
		$this->tags['thumbnail'] = $this->get_tag($arRules,$type = 'thumbnail');
		$this->tags['embed'] = $this->get_tag($arRules,$type = 'embed');
		
		if($this->tags['embed'] != ''){
			$this->tags['width'] = $arRules['width'];
			$this->tags['height'] = $arRules['height'];
			$this->tags['embedFrame'] = $arRules['embedFrame'];
			
			//MAIL.RU VIDEO FRAME HACK (there is no inline tags with embed video url on page)
			if(isset($arRules['embedFormat'])){
				$this->tags['embedFormat'] = $arRules['embedFormat'];
			}
			
			return true;
		}
		
		return false;
	}

	/**
	 * parses given contents from url by DOM lib
	 * @return bool
	 */
	private function parse_html()
	{
		$doc = new DOMDocument();
		libxml_use_internal_errors(true);
		$doc->loadHTML($this->html->content);
		libxml_clear_errors();
		
		//getting title nodes:
		$this->get_metas($doc);
		
		$this->embed = (isset($this->tags['embed'])) ? $this->get_optional($doc,$this->tags['embed']) : '';
		$this->thumbnail = (isset($this->tags['thumbnail'])) ? $this->get_optional($doc,$this->tags['thumbnail']) : '';
		
		//MAIL.RU VIDEO FRAME HACK (there is no inline tags with embed video url on page)
		if(isset($this->tags['embedFormat'])){
			$arSrc = explode("/",$this->embed);
			$this->embed = str_replace("{LAST}", end($arSrc), $this->tags['embedFormat']);
		}
		
		return $this->check();
	}

	/**
	 * format parsed contents to readable normalized view
	 * @return bool
	 */
	private function format_data()
	{
		$this->title = iconv("UTF-8", $this->charset, $this->title);
		$this->description = iconv("UTF-8", $this->charset, $this->description);
		
		$vars = array('{WIDTH}', '{HEIGHT}', '{SRC}');
		$vals = array($this->tags['width'], $this->tags['height'], $this->embed);
		
		$this->frame = str_replace($vars, $vals, $this->tags['embedFrame']);
		
		return (!empty($this->embed));
	}

	/**
	 * form tags arrays to use while parsing data
	 * @param        $arRules
	 * @param string $type
	 * @return bool
	 */
	private function get_tag($arRules,$type = '')
	{
		if(isset($arRules[$type])){
			$arSetting = explode(";",$arRules[$type]);
			
			if(count($arSetting)){
				
				foreach($arSetting as $strElement){
					$arElement = explode("/",$strElement);
					
					if(count($arElement)){
						$arSelector = explode("=",$arElement[1]);
						$el[$arElement[0]] = $arSelector;
						
						return $el;
					}
				}
				
			}
			
		}
		return false;
	}

	/**
	 * get optional tags values from provided by DOM html doc
	 * @param $doc
	 * @param $rules
	 * @return mixed
	 */
	private function get_optional($doc,$rules)
	{
		foreach($rules as $tag => $selector){
			$elements = $doc->getElementsByTagName($tag);
			
			for ($i = 0; $i < $elements->length; $i++){
				$element = $elements->item($i);
				
				if($element->getAttribute($selector[0]) == $selector[1]){
					
					if($selector[2] != "text") $res = $element->getAttribute($selector[2]);
					else $res = $element->nodeValue;
					
					return $res;
				}
				
			}
		}
	}

	/**
	 * get main meta tags as title and description from provided by DOM html doc
	 * i assume there are more elegant solutions to get meta tags such as get_meta_tags($url)
	 * @param $doc
	 */
	private function get_metas($doc)
	{
		$nodes = $doc->getElementsByTagName('title');
		$this->title = ($nodes->length>0) ? $nodes->item(0)->nodeValue : '';
		if($this->title == ''){
			$nodes = $doc->getElementsByTagName('h1');
			
			$this->title = ($nodes->length>0) ? $nodes->item(0)->nodeValue : $this->title;
			$this->title = ($this->title == '' && $this->tags['title']) ? $this->get_optional($doc,$this->tags['title']) : $this->title;
		}
		
		$metas = $doc->getElementsByTagName('meta');
		for ($i = 0; $i < $metas->length; $i++){
			$meta = $metas->item($i);
			
			if($meta->getAttribute('name') == 'description'){
				
				$this->description = $meta->getAttribute('content');
				$this->description = ($this->description == '' && $this->tags['description']) ? $this->get_optional($doc,$this->tags['description']) : $this->description;
			}
			
			if($meta->getAttribute('name') == 'keywords'){
				$this->keywords = $meta->getAttribute('content');
			}
		}
	}

	/**
	 * set usage example rules to response
	 * @return array
	 */
	static public function usage_rules()
	{
		return array(
			'request_url' => $_SERVER['HTTP_HOST'],
			'request_method' => 'POST||GET',
			'request_params' => array(
				'token' => 'aa312a17-1a9d-4f57-a2a7-589e91da6e1d',
				'url' => 'https://www.youtube.com/watch?v=Rqf3J4ZOPCw',
			)
		);
	}
}
